package com.lemonade.chat.data.remote

import android.os.SystemClock
import com.lemonade.chat.R
import com.lemonade.chat.data.interfaces.LemonadeDataSource
import com.lemonade.chat.model.Message
import io.reactivex.Flowable
import io.realm.Realm
import io.realm.RealmResults
import java.util.concurrent.TimeUnit

class LemonadeMockDataSource(private val jsonUtils: com.lemonade.chat.util.JsonUtils) : LemonadeDataSource {

    companion object {
        private const val SERVICE_LATENCY_IN_MILLIS = 2000
    }

    override fun step1FirstMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_1_first_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step1SecondMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_1_second_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step1Input(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_1_first_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step2FirstMessage(userName: String): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_2_first_message, Message::class.java)
        res.text = res.text?.replace("%s", userName)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step2SecondMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_2_second_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step3FirstMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_3_first_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step4FirstMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_4_first_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step4SecondMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_4_second_message, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun lastStepMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.last_step, Message::class.java)
        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun step4ThirdMessage(): Flowable<Message> {
        val res = jsonUtils.loadJSON<Message>(R.raw.step_4_third_message, Message::class.java)

        return Flowable.just(res)
                .delay(SERVICE_LATENCY_IN_MILLIS.toLong(), TimeUnit.MILLISECONDS)
                .doOnNext { saveMessage(it) }
    }

    override fun getMessages(): Flowable<RealmResults<Message>>? {
        return null!!
    }

    override fun sendMessage(msg: Message) {
        saveMessage(msg)
    }

    override fun saveMessage(msg: Message) {
        val currentTimeMillis = System.currentTimeMillis()
        msg.timestamp = currentTimeMillis
        msg.messageId = currentTimeMillis.toString()

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insert(msg)
        realm.commitTransaction()
    }
}
