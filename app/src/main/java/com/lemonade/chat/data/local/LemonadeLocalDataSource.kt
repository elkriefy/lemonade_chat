package com.lemonade.chat.data.local

import android.os.SystemClock
import com.lemonade.chat.data.interfaces.LemonadeDataSource
import io.reactivex.Flowable
import io.realm.RealmResults
import com.lemonade.chat.model.Message
import io.reactivex.BackpressureStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.BehaviorProcessor
import io.realm.Realm
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LemonadeLocalDataSource : LemonadeDataSource {

    var subject: BehaviorProcessor<RealmResults<Message>>? = BehaviorProcessor.create()
    internal var all: RealmResults<Message>? = null

    override fun getMessages(): Flowable<RealmResults<Message>>? {
        Flowable.create<List<Message>>({ _ ->
            val realm = Realm.getDefaultInstance()
            all = realm.where(Message::class.java).findAll()
            all!!.addChangeListener({ _ -> subject?.onNext(all!!) })
            if (all != null) {
                subject?.onNext(all!!)
            }
        }, BackpressureStrategy.BUFFER)
                .delay(5, TimeUnit.MILLISECONDS)
                ?.subscribeOn(AndroidSchedulers.mainThread())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({}, { Timber.e(it) })
        return subject
    }

    override fun step1FirstMessage(): Flowable<Message> {
        return null!!
    }

    override fun step1SecondMessage(): Flowable<Message> {
        return null!!
    }

    override fun step1Input(): Flowable<Message> {
        return null!!
    }

    override fun step2FirstMessage(userName: String): Flowable<Message> {
        return null!!
    }

    override fun step2SecondMessage(): Flowable<Message> {
        return null!!
    }

    override fun step3FirstMessage(): Flowable<Message> {
        return null!!
    }

    override fun step4FirstMessage(): Flowable<Message> {
        return null!!
    }

    override fun step4SecondMessage(): Flowable<Message> {
        return null!!
    }

    override fun step4ThirdMessage(): Flowable<Message> {
        return null!!
    }

    override fun lastStepMessage(): Flowable<Message> {
        return null!!
    }

    override fun sendMessage(msg: Message) {

    }

    override fun saveMessage(msg: Message) {
        val currentTimeMillis = System.currentTimeMillis()
        msg.timestamp = currentTimeMillis
        msg.messageId = currentTimeMillis.toString()

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insert(msg)
        realm.commitTransaction()
    }
}
