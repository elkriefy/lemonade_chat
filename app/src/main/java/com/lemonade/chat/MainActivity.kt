package com.lemonade.chat

import android.os.Bundle
import com.lemonade.chat.core.BaseLemonadeActivity
import com.lemonade.chat.fragments.MainFragment
import com.lemonade.chat.inject.component.ActivityComponent

class MainActivity : BaseLemonadeActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(R.id.container, MainFragment()).commit()
    }

    override fun intect(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }
}
