package com.lemonade.chat.core

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import com.lemonade.chat.inject.component.ActivityComponent
import com.lemonade.chat.inject.component.DaggerActivityComponent
import com.lemonade.chat.inject.modules.AppModule

abstract class BaseLemonadeActivity : AppCompatActivity() {

    abstract fun intect(activityComponent: ActivityComponent)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val component = DaggerActivityComponent.builder()
                .appModule(AppModule(application))
                .build()
        intect(component)
    }
}
